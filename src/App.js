import React from 'react';
import './App.css';
import {
    Router,
    Switch,
    Route
} from "react-router-dom";
import ChatPage from "./containers/ChatPage";
import Login from "./containers/LoginPage";
import Register from "./containers/RegisterPage";
import PrivateRoute from "./containers/PrivateRoute";
import history from "./utils/history";
import {Redirect} from 'react-router-dom';


function App() {
    return (
        <Router history={history}>
            <div className="App">
                <Switch>
                    <Route exact path="/login" component={Login}/>
                    <Route path={'/register'} component={Register}/>
                    <PrivateRoute path="/chat/:chatId?" component={ChatPage}/>
                    <Redirect to="/login"/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
