import { combineReducers } from "redux";
import authReducer from './authReducer'
import chats from './chatReducer'
import messagesReducer from "./messagesReducer";
import services from './services';

export default combineReducers({
    authReducer,
    chats,
    messagesReducer,
    services
})


export const getUserId = user => user._id;
export const getActiveUser = state => state.authReducer.user;

export const isCreator = (state, chat) => {
    try {
        return getUserId(chat.creator) === getUserId(getActiveUser(state));
    } catch (e) {
        return false;
    }
};

export const isMember = (state, chat) => {
    try {
        return chat.members.some(member => getUserId(member) === getUserId(getActiveUser(state)));
    } catch (e) {
        return false;
    }
};

export const isChatMember = (state, chat) => isCreator(state, chat) || isMember(state, chat);
