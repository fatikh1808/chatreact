import * as types from '../constants/auth';
import {callApi} from "../utils/callApi";

export function register(username, password) {
    return (dispatch, getState) => {
        const {isFetching} = getState().services;

        if (isFetching.signup) {
            return Promise.resolve();
        }

        dispatch({
            type: types.REGISTER_SUBMIT_REQUEST,
        });

        return callApi(
            '/signup',
            undefined,
            {method: 'POST'},
            {
                username,
                password
            },
        )
            .then(json => {
                if (!json.token) {
                    throw new Error('Token has not been provided');
                }

                localStorage.setItem('token', json.token)

                dispatch({
                    type: types.REGISTER_SUBMIT_SUCCESS,
                    payload: json,
                })
            })
            .catch(reason => dispatch({
                type: types.REGISTER_SUBMIT_FAILURE,
                payload: reason,
            }));
    };
}

export function login(username, password) {
    return (dispatch, getState) => {
        const {isFetching} = getState().services;

        if (isFetching.login) {
            return Promise.resolve();
        }

        dispatch({
            type: types.LOGIN_SUBMIT_REQUEST,
        })
        return callApi(
            '/login',
            undefined,
            {method: 'POST'},
            {username, password})
            .then(json => {
                if (!json.token) {
                    throw new Error('Token has not been provided');
                }

                localStorage.setItem('token', json.token)

                dispatch({
                    type: types.LOGIN_SUBMIT_SUCCESS,
                    payload: json,
                })
            }).catch(reason => dispatch({
                type: types.LOGIN_SUBMIT_FAILURE,
                payload: reason,
            }));
    };
}

export function logout() {
    return (dispatch, getState) => {
        const {isFetching} = getState().services;

        if (isFetching.logout) {
            return Promise.resolve();
        }

        dispatch({
            type: types.LOGOUT_SUBMIT_REQUEST,
        });
        return callApi('/logout')
            .then(json => {
                localStorage.removeItem('token');

                dispatch({
                    type: types.LOGOUT_SUBMIT_SUCCESS,
                    payload: json
                })
            })
            .catch(reason => dispatch({
                type: types.LOGOUT_SUBMIT_FAILURE,
                payload: reason,
            }));
    };
}

export function recieveAuth() {
    return (dispatch, getState) => {
        const {token} = getState().authReducer;

        dispatch({
            type: types.RECIEVE_AUTH_REQUEST,
        })

        return callApi('/users/me', token)
            .then(json => {
                dispatch({
                    type: types.RECIEVE_AUTH_SUCCESS,
                    payload: json,
                })
            }).catch(reason => dispatch({
                type: types.RECIEVE_AUTH_FAILURE,
                payload: reason,
            }));
    }
}