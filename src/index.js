import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from "./store";
import {Provider} from 'react-redux';

const rootEl = document.getElementById('root')
const store = configureStore();


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , rootEl);

if (module.hot) {
    module.hot.accept('./App', () => {
        ReactDOM.render(
            <Provider store={store}>
            <App/>
        </Provider>, rootEl);
    })
}

serviceWorker.unregister();
