import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    regPaper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    regAvatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    regForm: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    regSubmit: {
        margin: theme.spacing(15, 0, 2),
        marginBottom: 15,
        marginTop: 20
    },
    chatContent: {
        flexGrow: 1,
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(1),
        },
        [theme.breakpoints.up('m')]: {
            padding: theme.spacing(3),
        },
        [theme.breakpoints.up('lg')]: {
            padding: theme.spacing(4),
        },
    },
    barDrawer: {
        [theme.breakpoints.up('sm')]: {
            width: 240,
            flexShrink: 0,
        },
    },
    barAppBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${240}px)`,
            marginLeft: 240,
        },
    },
    barMenuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    barToolbar: theme.mixins.toolbar,
    barDrawerPaper: {
        width: 240,
    },
    inRoot: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        position: 'fixed',
        bottom: 0,
        top: "auto",
        left: 0,
        right: 0,
        [theme.breakpoints.up('lg')]: {
            paddingLeft: 240,
            height: 80
        },
    },
    inInput: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    inIconButton: {
        padding: 10,
    },
    chatExBtn: {
        [theme.breakpoints.down('sm')]: {
            left: 205
        },
        [theme.breakpoints.up('m')]: {
            left: 240
        },
        [theme.breakpoints.up('lg')]: {
            left: 1740
        },
    },
    chatMenu: {
        [theme.breakpoints.up('lg')]: {
            marginLeft: 200,
        },
    },
    messageWrapper: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 10,
    },
    messageWrappperFromMe: {
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    message: {
        maxWidth: '70%',
        minWidth: '10%',
        padding: theme.spacing.unit,
        marginLeft: theme.spacing(2),
        alignItems: 'center'

    },
    messageFromMe: {
        marginRight: theme.spacing(2),
        alignItems: 'center',
        backgroundColor: '#e6dcff',
    },
    statusMessage: {
        width: '100%',
        textAlign: 'center',
    },
    statusMessageUser: {
        display: 'inline',
    },
}));

export default useStyles;