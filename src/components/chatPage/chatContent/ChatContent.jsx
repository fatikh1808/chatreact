import React from 'react';
import useStyles from "../../useStyles/useStyles";
import ContentInput from "./contentInput/ContentInput";
import MessagesList from "./messagesList/MessagesList";


const ChatContent = (props) => {
    const classes = useStyles();
    const {
        messages,
        activeChat,
        activeUser,
        joinChat,
        sendMessage,
        isConnected,
    } = props;

    return (
        <main className={classes.chatContent}>
            <div className={classes.barToolbar}/>
            <MessagesList
                messages={messages}
                activeUser={activeUser}
            />
            {activeChat && (
                <ContentInput
                    disabled={!isConnected}
                    sendMessage={sendMessage}
                    showJoinButton={!activeUser.isChatMember}
                    onJoinButtonClick={() => joinChat(activeChat._id)}
                />
            )}
        </main>
    );
}

export default ChatContent;
