import useStyles from "../../../../useStyles/useStyles";
import InputBase from "@material-ui/core/InputBase";
import React from "react";

const Input = (props) => {

    const classes = useStyles();

    return(
        <InputBase
            className={classes.inInput}
            placeholder="write ur message"
            inputProps={{'aria-label': 'search google maps'}}
            onChange={props.onChange}
            value={props.value}
        />
    )
}

export default Input;