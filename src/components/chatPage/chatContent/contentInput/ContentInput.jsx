import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';
import useStyles from "../../../useStyles/useStyles";
import Input from "./input/Input";
import SendButton from "./button/Button";
import Button from '@material-ui/core/Button';


const ContentInput = (props) => {

    const classes = useStyles();
    const [content, setContent] = useState('')
    const {
        sendMessage,
        showJoinButton,
        onJoinButtonClick,
        disabled
    } = props;

    const onChange = (event) => {
        event.persist();
        setContent(event.target.value)
    }

    const enable = () => {
        return content === '';
    }

    const onSubmit = (event) => {
        event.preventDefault();
        sendMessage(content)
        setContent('')
    }

    return (
        <Paper component="form" variant="outlined" square className={classes.inRoot}>
            {showJoinButton ? (
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={disabled}
                    onClick={onJoinButtonClick}
                >
                    Join
                </Button>
            ) : (
                <React.Fragment>
                    <Input
                        onChange={onChange}
                        value={content}
                    />
                    <SendButton
                        disable={enable()}
                        onSubmit={onSubmit}
                    />
                </React.Fragment>
            )}
        </Paper>
    );
}

export default ContentInput;