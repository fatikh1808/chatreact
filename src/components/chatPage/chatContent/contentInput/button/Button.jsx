import useStyles from "../../../../useStyles/useStyles";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import React from "react";

const Button = (props) => {

    const classes = useStyles();

    const {disable, onSubmit} = props;

    return (
        <IconButton
            type="submit"
            className={classes.inIconButton}
            disabled={disable}
            onClick={onSubmit}
            color="primary"
        >
            <Icon fontSize="large">send</Icon>
        </IconButton>
    )
}

export default Button;