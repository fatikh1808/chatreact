import React from "react";
import Message from "../message/Message";
import Typography from '@material-ui/core/Typography';

class MessagesList extends React.Component {

    static defaultProps = {
        messages: [],
    };

    render() {

        let {
            messages,
            activeUser
        } = this.props;

        return messages && messages.length ? (
            <div
                style={{paddingBottom: 90}}
            >
                {messages.map(message => (
                    <Message key={message._id} activeUser={activeUser} {...message} />
                ))}
            </div>
        ) : (
            <Typography variant="subtitle1">There is no messages yet...</Typography>
        );
    }
}

export default MessagesList;