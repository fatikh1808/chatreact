import React from 'react';
import Box from "@material-ui/core/Box";
import senderName from "../../../../utils/sender-name";
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import randomColor from '../../../../utils/color-from';
import AvatarChat from "../../../../utils/Avatar";
import Paper from '@material-ui/core/Paper';
import useStyles from "../../../useStyles/useStyles";

const Message = (props) => {

    const {
        content,
        sender,
        activeUser,
        createdAt,
        statusMessage
    } = props;

    const classes = useStyles();

    const displayedName = senderName(sender);

    const flexControl = (sender) => {
        if (sender._id === activeUser._id){
            return "flex-end"
        } else {
            return "flex-start"
        }
    }
    const isMessageFromMe = sender._id === activeUser._id;

    const userAvatar = <AvatarChat colorFrom={sender._id}>{displayedName}</AvatarChat>;

    if (statusMessage) {
        return (
            <div className={classes.messageWrapper}>
                <Typography className={classes.statusMessage}>
                    <Typography
                        variant="caption"
                        style={{ color: randomColor(sender._id) }}
                        className={classes.statusMessageUser}
                    >
                        {displayedName}
                    </Typography>
                    {content}
                    <Typography
                        variant="caption"
                        component="span">
                        {moment(createdAt).fromNow()}
                    </Typography>
                </Typography>
            </div>
        );
    }

    return (
        <Box
             display="flex"
             justifyContent={flexControl(sender)}
             className={isMessageFromMe ? classes.messageWrappperFromMe : classes.messageWrapper}
             m={1}
             p={1}
             bgcolor="background.paper"
        >
            {!isMessageFromMe && userAvatar}
            <Paper className={isMessageFromMe ? classes.messageFromMe : classes.message}>
            <Typography
                variant="caption"
                style={{ color: randomColor(sender._id) }}
                className={classes.statusMessageUser}
            >
                {displayedName}
            </Typography>
            <Typography variant={'body1'} maxWidth="75%">
                {content}
            </Typography>
            <Typography variant="caption">
                {moment(createdAt).fromNow()}
            </Typography>
            </Paper>
            {isMessageFromMe && userAvatar}
        </Box>
    );
}

export default Message;