import List from "@material-ui/core/List";
import ChatListItem from "./chatListItem";
import Typography from "@material-ui/core/Typography";
import React from "react";

const ChatList = (props) => {

    const {chats, activeChat, disabled,} = props;

    return (
        <List>
            {chats && chats.length ? (
                chats.map(chat => (
                    <ChatListItem
                        disabled={disabled}
                        key={chat._id}
                        active={Boolean(activeChat && activeChat._id === chat._id)}
                        chatId={chat._id}
                        {...chat}
                    />
                ))
            ) : (
                <Typography variant="subtitle1">
                    There is no chats yet...
                </Typography>
            )}
        </List>
    )
}

export default ChatList;

