import React from 'react';
import moment from 'moment';
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AvatarChat from "../../../../../utils/Avatar";

const useStyles = makeStyles((theme) => ({
    activeItem: {
        backgroundColor: theme.palette.grey[200],
    },
}));

const ChatListItem = (props) => {
    const {
        disabled,
        title,
        chatId,
        active,
        createdAt,
    } = props;

    const classes = useStyles();


    return (
        <ListItem
            button
            component={Link}
            to={`/chat/${chatId}`}
            className={active ? classes.activeItem : ''}
            disabled={disabled}
        >
            <AvatarChat colorFrom={chatId}>{title}</AvatarChat>
            <ListItemText primary={title} secondary={moment(createdAt).fromNow()}/>
        </ListItem>
    )
}


export default ChatListItem;
