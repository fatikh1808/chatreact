import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
    newChatButton: {
        position: 'absolute',
        left: 'auto',
        right: theme.spacing(3),
        bottom: 100,
    },
    modalWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        width: '30%',
        minWidth: '300px',
        padding: theme.spacing(3),
    },
}));

const NewChatButton = (props) => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const [title, setTitle] = useState('');
    const [isValid, setIsValid] = useState(false);

    const toggleModal = () => {
        setIsOpen(!isOpen)
    };

    const titleChange = (event) => {
        setTitle(event.target.value)
        setIsValid(!isValid);
    };

    const createClick = (event) => {
        event.preventDefault();

        if (!title) {
            setTitle(title)
            setIsValid(false)
        }

        props.onClick(title);
        toggleModal();
        setTitle('')
        setIsValid(true)
    };

    const {disabled} = props;

    return (
        <React.Fragment>
            <Fab
                color="secondary"
                aria-label="add"
                disabled={disabled}
                className={classes.newChatButton}
                onClick={toggleModal}
            >
                <AddIcon />
            </Fab>
            <Modal
                open={isOpen} className={classes.modalWrapper}
                onClose={toggleModal}>
                <Paper>
                    <Typography variant="button" id="modal-title">
                        Create new chat
                    </Typography>
                    <TextField
                        fullWidth
                        label="New chat"
                        placeholder="Type the title..."
                        type="text"
                        margin="normal"
                        autoComplete="new-chat"
                        value={title.value}
                        onChange={titleChange}
                        error={!title.isValid}
                    />
                    <Button color="primary" onClick={createClick}>
                        Create
                    </Button>
                </Paper>
            </Modal>
        </React.Fragment>
    );
}

export default NewChatButton;
