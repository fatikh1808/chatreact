import useStyles from "../../../useStyles/useStyles";
import {useTheme} from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import Drawer from "@material-ui/core/Drawer";
import TextField from "@material-ui/core/TextField";
import ChatList from "./chatList/ChatList";
import NewChatButton from "./newChatButton/NewChatButton";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import RestoreIcon from "@material-ui/icons/Restore";
import ExploreIcon from "@material-ui/icons/Explore";
import React from "react";

const SideBar = (props) => {

    const {
        chats,
        activeTab,
        container,
        createChat,
        filterChats,
        handleTabChange,
        searchChange,
        mobileOpen,
        handleDrawerToggle,
        isConnected,
        searchValue
    } = props;

    const classes = useStyles();
    const theme = useTheme();

    return(
        <nav className={classes.barDrawer} aria-label="mailbox folders">
            <Hidden smUp implementation="css">
                <Drawer
                    container={container}
                    variant="temporary"
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    classes={{
                        paper: classes.barDrawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true,
                    }}
                >
                    <TextField
                        label="Search"
                        id="margin-normal-first"
                        margin="normal"
                        style={{
                            marginLeft: 20,
                            paddingTop: 0,
                            paddingRight: 20,
                            paddingBottom: 0,
                        }}
                        value={searchValue}
                        onChange={searchChange}
                    />
                    <ChatList
                        disabled={!isConnected}
                        chats={filterChats(activeTab === 0 ? chats.my : chats.all)}
                        activeChat={chats.active}
                    />
                    <NewChatButton
                        disabled={!isConnected}
                        onClick={createChat}
                    />
                    <BottomNavigation
                        value={activeTab}
                        onChange={handleTabChange}
                        showLabels
                    >
                        <BottomNavigationAction label="My Chats" icon={<RestoreIcon/>}/>
                        <BottomNavigationAction label="Explore" icon={<ExploreIcon/>}/>
                    </BottomNavigation>
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                        paper: classes.barDrawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    <TextField
                        label="Search"
                        id="margin-normal-second"
                        margin="normal"
                        style={{
                            marginLeft: 20,
                            paddingTop: 0,
                            paddingRight: 20,
                            paddingBottom: 0,
                        }}
                        value={searchValue}
                        onChange={searchChange}
                    />
                    <ChatList
                        disabled={!isConnected}
                        chats={filterChats(activeTab === 0 ? chats.my : chats.all)}
                        activeChat={chats.active}
                    />
                    <NewChatButton
                        disabled={!isConnected}
                        onClick={createChat}
                    />
                    <BottomNavigation
                        value={activeTab}
                        onChange={handleTabChange}
                        showLabels
                        style={{
                            top: "auto",
                            left: 0,
                            bottom: 0,
                            display: "flex",
                            marginBottom: 10,
                            position: "fixed",
                            marginLeft: 30
                        }}>
                        <BottomNavigationAction label="My Chats" icon={<RestoreIcon/>}/>
                        <BottomNavigationAction label="Explore" icon={<ExploreIcon/>}/>
                    </BottomNavigation>
                </Drawer>
            </Hidden>
        </nav>
    )
}
export default SideBar;
