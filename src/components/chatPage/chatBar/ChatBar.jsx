import React, {useState} from 'react';
import TopBar from "./topBar/TopBar";
import SideBar from "./sideBar/SideBar";

const ChatBar = (props) => {
    const {
        window,
        logout,
        chats,
        createChat,
        isConnected,
        activeChat,
        activeUser,
        deleteChat,
        editUser,
        leaveChat
    } = props;

    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [searchValue, setSearchValue] = useState('');
    const [activeTab, setActiveTab] = useState(0);

    const searchChange = (event) => {
        setSearchValue(event.target.value)
    };

    const handleTabChange = (event, value) => {
        setActiveTab(value)
    };

    const filterChats = (chats) => {

        return chats
            .filter(chat => chat.title.toLowerCase().includes(searchValue.toLowerCase()))
            .sort((one, two) => (one.title.toLowerCase() <= two.title.toLowerCase() ? -1 : 1));
    };

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <div>
            <TopBar
                logout={logout}
                activeChat={activeChat}
                activeUser={activeUser}
                deleteChat={deleteChat}
                editUser={editUser}
                leaveChat={leaveChat}
                isConnected={isConnected}
                handleDrawerToggle={handleDrawerToggle}
            />
            <SideBar
                container={container}
                filterChats={filterChats}
                handleTabChange={handleTabChange}
                searchChange={searchChange}
                activeTab={activeTab}
                chats={chats}
                createChat={createChat}
                mobileOpen={mobileOpen}
                handleDrawerToggle={handleDrawerToggle}
                searchValue={searchValue}
                isConnected={isConnected}
            />
        </div>
    );
}

export default ChatBar;
