import useStyles from "../../../useStyles/useStyles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import Typography from "@material-ui/core/Typography";
import ChatMenu from "./chatMenu/ChatMenu";
import UserMenu from "./userMenu/UserMenu";
import AvatarChat from "../../../../utils/Avatar";
import Box from '@material-ui/core/Box';

const TopBar = (props) => {
    const {
        logout,
        activeChat,
        activeUser,
        deleteChat,
        editUser,
        handleDrawerToggle,
        isConnected,
        leaveChat
    } = props;
    const classes = useStyles();

    return (
        <AppBar position="fixed" className={classes.barAppBar}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                    className={classes.barMenuButton}
                >
                    <MenuIcon/>
                </IconButton>
                <Box component="div" className={classes.chatMenu} display="flex">
                {activeChat ? (
                    <React.Fragment>
                        <AvatarChat colorFrom={activeChat._id}>{activeChat.title}</AvatarChat>
                        <Typography>
                            {activeChat.title}
                            <ChatMenu
                                disabled={!isConnected}
                                activeUser={activeUser}
                                onLeaveClick={() => leaveChat(activeChat._id)}
                                onDeleteClick={() => deleteChat(activeChat._id)}
                            />
                        </Typography>
                    </React.Fragment>
                ) : (
                    <Typography variant="h4">
                        Chat
                    </Typography>
                )}
                </Box>
                <UserMenu
                    disabled={!isConnected}
                    activeUser={activeUser}
                    onLogoutClick={logout}
                    onEditProfileClick={editUser}
                />
            </Toolbar>
        </AppBar>
    )
}

export default TopBar;
