import React from 'react';
import ChatContent from "./chatContent/ChatContent";
import ChatBar from "./chatBar/ChatBar";

class ChatPage extends React.Component {

    componentDidMount() {
        const {
            match,
            fetchAllChats,
            fetchMyChats,
            setActiveChat,
            socketsConnect,
            mountChat,
        } = this.props;

        Promise.all([fetchAllChats(), fetchMyChats()])
            .then(() => {
                socketsConnect();
            })
            .then(() => {
                const { chatId } = match.params;
                // If we pass a chatId, then fetch messages from chat
                if (chatId) {
                    setActiveChat(chatId);
                    mountChat(chatId);
                }
            });
    }

    componentWillReceiveProps(nextProps) {
        const {
            match: { params }, setActiveChat, unmountChat, mountChat,
        } = this.props;

        const { params: nextParams } = nextProps.match;

        // If we change route, then fetch messages from chat by chatID
        if (nextParams.chatId && params.chatId !== nextParams.chatId) {
            setActiveChat(nextParams.chatId);
            unmountChat(params.chatId);
            mountChat(nextParams.chatId);
        }
    }

    render() {

        const {
            chats,
            setActiveChat,
            logout,
            createChat,
            deleteChat,
            sendMessage,
            editUser,
            joinChat,
            leaveChat,
            messages,
            activeUser,
            isConnected
        } = this.props;

        return (
            <div style={{display: "flex"}}>
                <ChatBar
                    logout={logout}
                    deleteChat={deleteChat}
                    leaveChat={leaveChat}
                    createChat={createChat}
                    setActiveChat={setActiveChat}
                    editUser={editUser}
                    isConnected={isConnected}
                    activeUser={activeUser}
                    activeChat={chats.active}
                    chats={chats}
                />
                <ChatContent
                    sendMessage={sendMessage}
                    joinChat={joinChat}
                    messages={messages}
                    activeChat={chats.active}
                    activeUser={activeUser}
                    isConnected={isConnected}
                />
            </div>
        );

    }
}

export default ChatPage;
