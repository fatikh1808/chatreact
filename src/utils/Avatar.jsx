import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import getColor from '../utils/color-from';
import titleInitials from '../utils/title-initials';

const AvatarChat = ({ colorFrom, children, ...rest }) => (
  <Avatar
      style={{
        backgroundColor: getColor(colorFrom),
        marginRight: 15 }}
      {...rest}>
    {titleInitials(children)}
  </Avatar>
);

export default AvatarChat;
