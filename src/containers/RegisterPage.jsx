import { connect } from "react-redux";
import RegisterPage from "../components/authPage/registerPage/RegisterPage";
import { bindActionCreators } from "redux";
import {register} from "../actions";

const mapDispatchToProps = dispatch => bindActionCreators({
    register,
}, dispatch);

const mapStateToProps = state => ({
    isAuthenticated: state.authReducer.isAuthenticated,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterPage);