import {connect} from "react-redux";
import ChatPage from "../components/chatPage/ChatPage";
import {bindActionCreators} from "redux";
import {
    fetchAllChats,
    fetchMyChats,
    setActiveChat,
    createChat,
    deleteChat,
    joinChat,
    leaveChat,
    editUser,
    sendMessage,
    logout,
    mountChat,
    unmountChat,
    socketsConnect
} from "../actions";
import * as fromChats from '../reducers/chatReducer'
import * as fromState from '../reducers';

const mapStateToProps = (state) => {
    const activeChat = fromChats.getById(state.chats, state.chats.activeId);

    return {
        isAuthenticated: state.authReducer.isAuthenticated,
        chats: {
            active: activeChat,
            my: fromChats.getByIds(state.chats, state.chats.myIds),
            all: fromChats.getByIds(state.chats, state.chats.allIds),
        },
        activeUser: {
            ...state.authReducer.user,
            isMember: fromState.isMember(state, activeChat),
            isCreator: fromState.isCreator(state, activeChat),
            isChatMember: fromState.isChatMember(state, activeChat),
        },
        messages: state.messagesReducer,
        error: state.services.errors.chat,
        isConnected: state.services.isConnected,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchAllChats,
            fetchMyChats,
            setActiveChat,
            logout,
            createChat,
            deleteChat,
            joinChat,
            leaveChat,
            editUser,
            sendMessage,
            mountChat,
            unmountChat,
            socketsConnect,
        },
        dispatch,
    );

export default connect(mapStateToProps, mapDispatchToProps)(ChatPage);
