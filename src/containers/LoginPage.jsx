import { connect } from "react-redux";
import LoginPage from "../components/authPage/loginPage/LoginPage";
import { bindActionCreators } from "redux";
import {login} from "../actions";

const mapDispatchToProps = dispatch => bindActionCreators({
    login
}, dispatch);

const mapStateToProps = state => ({
    isAuthenticated: state.authReducer.isAuthenticated,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage);